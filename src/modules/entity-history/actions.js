import { callApi } from '../api';
import actionTypes from './action-types';
import * as getters from './getters';
import encodeDate from '../../util/encode-date';

export function incrementHistory(reactor) {
  reactor.dispatch(actionTypes.ENTITY_HISTORY_INCREMENT, {});
}

export function selectPeriod(reactor, period) {
  reactor.dispatch(actionTypes.ENTITY_HISTORY_PERIOD_SELECTED, { period });
}

export function fetchRecent(reactor, entityId = null) {
  reactor.dispatch(actionTypes.RECENT_ENTITY_HISTORY_FETCH_START, {});

  let url = 'history/period';

  if (entityId !== null) {
    url += `?filter_entity_id=${entityId}`;
  }

  return callApi(reactor, 'GET', url).then(
    stateHistory => reactor.dispatch(
      actionTypes.RECENT_ENTITY_HISTORY_FETCH_SUCCESS, { stateHistory }),

    () => reactor.dispatch(actionTypes.RECENT_ENTITY_HISTORY_FETCH_ERROR, {})
  );
}

export function getSelectedPeriod(reactor) {
   // I clearly don't understand what's going on here
  return reactor.evaluate(getters.selectedPeriod) || reactor.serialize().currentEntityHistoryPeriod;
}

export function fetchSelectedPeriod(reactor) {
  fetchPeriod(reactor, getSelectedPeriod(reactor));
}

export function fetchPeriod(reactor, period) {
  reactor.dispatch(actionTypes.ENTITY_HISTORY_FETCH_START, { period });

  return callApi(reactor, 'GET', `history/period/?start=${encodeDate(period.start)}&end=${encodeDate(period.end)}`).then(
    stateHistory => reactor.dispatch(
      actionTypes.ENTITY_HISTORY_FETCH_SUCCESS, { period, stateHistory }),

    () => reactor.dispatch(actionTypes.ENTITY_HISTORY_FETCH_ERROR, {})
  );
}
