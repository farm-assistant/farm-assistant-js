import { Store, toImmutable } from 'nuclear-js';
import dateToStr from '../../../util/date-to-str';
import actionTypes from '../action-types';

const INSTANCE = new Store({
  initialize() {
    /* eslint-disable no-use-before-define */
    this.on(actionTypes.ENTITY_HISTORY_PERIOD_SELECTED, periodSelected);
    this.on(actionTypes.LOG_OUT, logOut);
    /* eslint-enable no-use-before-define */
  },
});

export default INSTANCE;

function periodSelected(state, { period }) {
  console.log(period);
  return period;
}

function logOut() {
  return INSTANCE.getInitialState();
}
