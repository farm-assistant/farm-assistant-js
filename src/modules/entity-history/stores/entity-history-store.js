import { Store, toImmutable } from 'nuclear-js';
import actionTypes from '../action-types';
import model from '../../entity/model';
import parseDate from '../../../util/parse-date-time-str';
import apiActionTypes from '../../rest-api/action-types';

const INSTANCE = new Store({
  getInitialState() {
    return toImmutable({});
  },

  initialize() {
    /* eslint-disable no-use-before-define */
    this.on(actionTypes.ENTITY_HISTORY_FETCH_SUCCESS, entriesLoaded);
    this.on(apiActionTypes.API_FETCH_SUCCESS, apiUpdate);
    this.on(actionTypes.LOG_OUT, logOut);
    /* eslint-enable no-use-before-define */
  },
});

export default INSTANCE;

/**
 * @param {Immutable.Map} state
 * @param {Object} payload
 * @param {Model} payload.model
 * @param {any} payload.params
 * @param {Object|Array} payload.result
 */
function apiUpdate(state, { model, result, params }) {
  // not ready yet
  return state;
  if (result.last_changed === undefined) {
    return state;
  }
  const period = window._hass.entityHistoryActions.getSelectedPeriod();
  const updatedAt = parseDate(result.last_changed);
  if (updatedAt > period.start && updatedAt < period.end) {
    const mutableState = state.toJS();
    const entityId = result.entity_id;
    const newState = {
      updated_at: updatedAt.getTime() / 1000,
      value: result.value,
    };
    if (!mutableState[entityId]) {
      mutableState[entityId] = {
        friendly_name: result.attributes.friendly_name,
        latitude: result.attributes.latitude,
        longitude: result.attributes.longitude,
        unit_of_measurement: result.attributes.unit_of_measurement,
        states: [newState],
      };
    } else {
      mutableState[entityId].states.push(newState);
    }
    return toImmutable(mutableState);
  }
  return state;
}

function entriesLoaded(state, { period, stateHistory }) {
  return toImmutable(stateHistory);
}

function logOut() {
  return INSTANCE.getInitialState();
}
