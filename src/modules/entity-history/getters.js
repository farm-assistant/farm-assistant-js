import { toImmutable } from 'nuclear-js';

export const isLoadingEntityHistory = [
  'isLoadingEntityHistory',
];

export const selectedPeriod = [
  'selectedEntityHistoryPeriod',
];

export const entityHistoryMap = [
  'entityHistory',
];

export const recentEntityHistoryMap = [
  'recentEntityHistory',
];

export const recentEntityHistoryUpdatedMap = [
  'recentEntityHistory',
];
