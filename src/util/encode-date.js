export default function encodeDate(date) {
  return encodeURIComponent(new Date(date.toUTCString()).toISOString());
}
